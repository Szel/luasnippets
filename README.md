## Description ##
This repo contains useful (and not) code snippets for Lua api in Transformice.

## Issues ##
[Bugs/proposals you can submit here](https://bitbucket.org/Szel/luasnippets/issues)

## Contact ##
[In game contact](http://atelier801.com/new-dialog?ad=Szel)

## License ##
Feel free to use it as you want.

## List of snippets ##
* [Ban script](https://bitbucket.org/Szel/luasnippets/src/master/banScript.lua) - banning users for a specified period of time