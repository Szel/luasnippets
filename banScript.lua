local bannedUsers = {}
function banUser(name, seconds, reason)
    bannedUsers[name] = {
        banUntil = os.time() + seconds * 1000,
        reason = reason
    }
    if eventOnBan then
        eventOnBan(name)
    end
end

function unbanUser(name)
    bannedUsers[name] = nil
    if eventOnUnban then
        eventOnUnban(name)
    end
end

function updateBanned()
    local now = os.time() -- cache time to spare processor
    local toRemove = {}
    for n, v in pairs(bannedUsers) do
        if v.banUntil < now then
            table.insert(toRemove, n)
        end
    end
    for i, v in ipairs(toRemove) do
        unbanUser(v)
    end
    return true
end

function protectFromInfidels(name, func, args, banFunc, banFuncArgsArgs)
    updateBanned()
    if not bannedUsers[name] then
        func(table.unpack(args or {}))     
    elseif banFunc then
        banFunc(table.unpack(banFuncArgsArgs or {}))
    end
end

--------------
-- How to use:

-- set up events
function eventOnBan(name)
    -- do something when person banned eg. show mesage
    print("Person " .. name .. " has been banned. Cause: " .. (bannedUsers[name].reason or "") .. "| Until: " .. os.date("%c", bannedUsers[name].banUntil))
end

function eventOnUnban(name)
    print("Person " .. name .. " has been unbanned")
end

-- some dummy function
function printName(name, word)
    print(name .. " is " .. word)
end
-- ban someone
banUser("someUser", 10)

-- call your function using condom
protectFromInfidels("someUser", printName, {"someUser", "nice"})
-- third parameter is a function wich will be called if "someUser" is banned
protectFromInfidels("someUser", printName, {"someUser", "nice"}, printName, {"someUser", "bad"})

-- or check in function
function printName(name, word)
    if updateBanned() and not bannedUsers[name] then
        print(name .. " is " .. word)
    else
        print()
    end
end

-- you can move updateBanned to eventLoop
function eventLoop(time1, time2)
    updateBanned()
end

function printName(name, word)
    if not bannedUsers[name] then
        print(name .. " is " .. word)
    end
end